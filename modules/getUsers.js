/*
	This function creates a user object for each user that is identified from the user.txt file

	This function requires the array/object with usernames only as the parameter

	It returns a callback function called done that is as follows 

	{
		success: Boolean, 
		message: String, 
		data: [this variable's type is dependent on what is return. For this file it is an object]
	}
*/

'use strict'

const _ = require('lodash')
const queue = require('async/queue')
const typeChecker = require('./typeChecker')

module.exports = (arr, done) => {
	//checking if the parameter provided is of the required type
	if (!typeChecker(arr, 'object')) {
		return done({
			success: false, 
			message: 'Please submit a parameter that is an javascript array or object', 
			data: null
		})
	}

	//checking if done is a function
	if (!typeChecker(done, 'function')) {
		return console.log('Please use the callback parameter as a function')
	}

	//checking if the parameter is not falsey
	if (!arr) {
		return done({
			success: false, 
			message: 'Please submit the array of arrays with users', 
			data: null
		})
	}
	else {
		var users = [] //declaring the users as an empty array that will be return

		//using the queue library to go through each line in the array
		//first the queue is defined and instantiated as to what it does
		//then the lines are pushed to the queue
		//then drain is called once the queue is complete, returning a callback (done) function
		//
		
		var q = queue((line, callback) => {
			line = _.compact(line) //removing falsey values from the parameter provided
			
			for (var i = 0; i < line.length; i++) {
				const j = _.findIndex(users, {user: line[i]}) //checking if the user is already added to the users array

				//if user is not added, add the user
				if (j === -1) {
					users.push({
						user: line[i], 
						follows: [line[i]]
					})
				}
			}

			callback()
		}, arr.length)

		q.push(arr, err => {
			//if the callback in queue function returns an error, 
			//it is catured here and console logged
			//this error will not stop the program, however
			if (err) {
				return console.log(err)
			}
			else {
				return
			}
		})

		q.drain = () => {
			//sort users alphabetically
			users.sort((a, b) => {
				var userA = a.user, userB = b.user

				if (userA < userB) return -1

				if (userA > userB) return 1

				return 0
			})

			done({
				success: true, 
				message: null, 
				data: users
			})
		}
	}
}