'use strict'

const chai = require('chai')
const expect = chai.expect
const runFeed = require('./../modules/runFeed')

//the users array that should be returned by the getUsers function
const users = [
	{user: 'Alan', follows: ['Alan']}, 
	{user: 'Martin', follows: ['Martin']}, 
	{user: 'Ward', follows: ['Ward']}, 
]

//the users array that should be returned by the getTweets function
const tweets = [
	{user: 'Alan', tweet: 'If you have a procedure with 10 parameters, you probably missed some.'},
	{user: 'Ward', tweet: 'There are only two hard things in Computer Science: cache invalidation, naming things and off-by-1 errors.'},
	{user: 'Alan', tweet: 'Random numbers should not be generated with a method chosen at random.'},
]

describe('renders the feed of each user and returns true if successful', () => {
	it('If parameters valid: should return an array with undefined values', done => {
		const feed = runFeed(users, tweets)
		
		expect(feed).to.be.a('array')
		expect(feed[0]).to.be.undefined

		done()
	})

	it('If parameters invalid: should return an object with keys: success, a boolean, message, a string and data, an null', done => {
		runFeed(null, null, callback => {
			expect(callback).to.be.a('object')
			expect(callback).to.have.own.property('success')
			expect(callback).to.have.own.property('message')
			expect(callback).to.have.own.property('data')

			expect(callback.success).to.be.a('boolean')
			expect(callback.message).to.be.a('string')
			expect(callback.data).to.be.a('null')

			done()
		})
	})
})
