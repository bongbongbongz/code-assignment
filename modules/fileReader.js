/*
	This file has the file reader function

	The file reader requires a path and a callback function as parameters

	The file reader function only reads from the user.txt file and tweet.txt file

	The file returns a callback function with an object that is as follows
	
	{
		success: Boolean, 
		message: String, 
		data: [this variable's type is dependent on what is return. For this file it is an object]
	}
*/

'use strict'

const fs = require('fs')
const _ = require('lodash')
const queue = require('async/queue')
const typeChecker = require('./typeChecker')

module.exports = (path, callback) => {
	//check if the path is type string
	if (!typeChecker(path, 'string')) {
		//returning the error
		return callback({
			success: false, 
			message: 'Please enter a path name of type string', 
			data: null
		})
	}

	//the callback must be a function
	//if callback is not a function, the program will halt and not complete
	if (!typeChecker(callback, 'function')) {
		return console.log('Please use the callback parameter as a function')
	}

	//this function is only meant to read from two files only
	//here we check if the path is of a known path or not
	if (path !== 'data/tweet.txt' && path !== 'data/user.txt') {
		return callback({
			success: false, 
			message: 'Please enter a valid path name', 
			data: null
		})
	}
	else {
		//using nodejs file system, the file is read using the path provided
		fs.readFile(path, (err, file) => {
			if (err) {
				console.log(err)
				return callback({
					success: false, 
					message: 'Error reading file', 
					data: null
				})
			}
			else {
				const arr = file.toString().split('\n') //here the buffer is converted to a string formatted that can be used for our purpose
				
				if (arr) {
					callback({
						success: true, 
						message: null, 
						data: arr
					})
				}
				else {
					callback({
						success: false, 
						message: 'Failed to read file', 
						data: null
					})
				}
			}
		})
	}
}