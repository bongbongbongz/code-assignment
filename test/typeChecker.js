'use strict'

const chai = require('chai')
const expect = chai.expect
const typeChecker = require('./../modules/typeChecker')

describe('validates the type of the input against the required type', () => {
	it('should return a boolean, true if the input and type match, false otherwise', done => {
		
		const value = typeChecker('data/tweet.txt', 'string')
		
		expect(value).to.be.a('boolean')
		
		done()
	})
})
