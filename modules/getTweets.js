/*
	This function sorts tweets received from the file reader and stores them in an array of objects
	for example: {user: 'tweet'}

	This function requires the tweets as an array from the file reader function as a parameter

	It returns a callback function called done that is as follows 

	{
		success: Boolean, 
		message: String, 
		data: [this variable's type is dependent on what is return. For this file it is an object]
	}
*/

'use strict'

const _ = require('lodash')
const queue = require('async/queue')
const typeChecker = require('./typeChecker')

module.exports = (tweetsArray, done) => {
	//checking if the parameter provided is of the required type
	if (!typeChecker(tweetsArray, 'object')) {
		return done({
			success: false, 
			message: 'Please submit a parameter that is an javascript array or object', 
			data: null
		})
	}

	//checking if done is a function
	if (!typeChecker(done, 'function')) {
		return console.log('Please use the callback parameter as a function')
	}

	//checking if the parameter is not falsey
	if (!tweetsArray) {
		return done({
			success: false, 
			message: 'Please submit the array containing tweets', 
			data: null
		})
	}
	else {
		var tweets = [] //declaring the tweets as an empty array that will be return
		tweetsArray = _.compact(tweetsArray) //removing falsey values from the parameter provided

		//using the queue library to go through each line in the array
		//first the queue is defined and instantiated as to what it does
		//then the lines are pushed to the queue
		//then drain is called once the queue is complete, returning a callback (done) function
		//
		
		var q = queue((line, callback) => {
			line = line.replace('\r', '') //remove the return key
				.split('>') //remove the greater than arrow
			
			//add the user and their tweet to the tweets array
			tweets.push({
				user: line[0], 
				tweet: line[1]
			})
			
			callback()
		})

		q.push(tweetsArray, err => {
			//if the callback in queue function returns an error, 
			//it is catured here and console logged
			//this error will not stop the program, however
			if (err) {
				return console.log(err)
			}
			else {
				return
			}
		})

		q.drain = () => {
			done({
				success: true, 
				message: null, 
				data: tweets
			})
		}
	}
}

