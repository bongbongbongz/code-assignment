/*
	This file has the type checker function

	Although it is only one line of code and is very short, 
	it is important that this is only written once and imported wherever it is needed

	Doing so make it easy to update this code, knowing all the other parts
	that require it, use it in a similar fashion, therefore updating it does not 
	break the program, as long as it checks the type and returns a boolean

	The function requires input, the data that needs to be evaluated and
	type that the data is evaluated against and must be type String

	The function returns a Boolean
*/

'use strict'

module.exports = (input, type) => {
	//checking if the type provided is of type String
	if (typeof(type) !== 'string') {
		return false
	}
	else {
		return typeof(input) === type
	}
}