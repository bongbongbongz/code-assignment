"use strict"

const express = require('express')
const app = express()
const path = require('path')
const morgan = require('morgan')
const bodyParser = require('body-parser')
const methodOverride = require('method-override')

//modules required to run this program
const fileReader = require('./modules/fileReader')
const lineToArray = require('./modules/lineToArray')
const getUsers = require('./modules/getUsers')
const getFollows = require('./modules/getFollows')
const getTweets = require('./modules/getTweets')
const runFeed = require('./modules/runFeed')

//middleware
app.use(morgan('dev'))
app.use(bodyParser.json({limit: '3mb'}));
app.use(bodyParser.urlencoded({limit: '3mb', extended: true}));
app.use(bodyParser.json())
app.use(bodyParser.json({type: 'application/vnd.api+json'}))
app.use(methodOverride())

app.listen(7001)

app.get('*', (req, res) => res.status(200).send('Hello World!'))

fileReader('data/user.txt', file => {
	if (!file.success) {
		return console.log(file.message)
	}
	else {

		lineToArray(file.data, linesArray => {
			if (!linesArray.success) {
				return console.log(linesArray.message)
			}
			else {
				
				getUsers(linesArray.data, users => {
					if (!users.success) {
						return console.log(users.message)
					}
					else {

						getFollows(users.data, linesArray.data, usersAndFollows => {
							if (!usersAndFollows.success) {
								return console.log(usersAndFollows.message)
							}
							else {

								fileReader('data/tweet.txt', tweetsArray => {
									if (!tweetsArray.success) {
										return console.log(tweetsArray.message)
									}
									else {

										getTweets(tweetsArray.data, tweets => {
											return tweets.success ? runFeed(users.data, tweets.data) : console.log(tweets.message)
										})
									}
								})
							}
						})
					}
				})
			}
		})
	}
})
