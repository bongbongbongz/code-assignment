/*
	This file gets the users each user is following into the follows array for each user

	The get follows function requires the users object, lines object/array and done as parameters

	The get follows function returns a done function with an object that is as follows
	
	{
		success: Boolean, 
		message: String, 
		data: [this variable's type is dependent on what is return. For this file it is an object]
	}
*/

'use strict'

const _ = require('lodash')
const queue = require('async/queue')
const typeChecker = require('./typeChecker')

module.exports = (users, lines, done) => {
	//checking if the users and lines parameters are of the required type
	if (!typeChecker(users, 'object') || !typeChecker(lines, 'object')) {
		return done({
			success: false, 
			message: 'Please submit a parameters that are an javascript array or object', 
			data: null
		})
	}

	//checking if done is a function
	if (!typeChecker(done, 'function')) {
		return console.log('Please use the callback parameter as a function')
	}

	//checking if the parameters users and lines are not falsey
	if (!users || !lines) {
		return done({
			success: false, 
			message: 'Please submit the array with user objects and the array with user lines', 
			data: null
		})
	}
	else {
		//using the queue library to go through each line in the array
		//first the queue is defined and instantiated as to what it does
		//then the lines are pushed to the queue
		//then drain is called once the queue is complete, returning a callback (done) function

		var q = queue((line, callback) => {
			line = _.compact(line) //removing falsey values

			for (var i = 0; i < line.length; i++) {
				//capturing the index of the user doing the following
				const j = _.findIndex(users, {user: line[0]}) 

				//if i is not at the user doing the following
				if (i !== 0) {
					//check if this user is not already in the follows array
					const followIndex = _.indexOf(users[j].follows, line[i])

					if (followIndex === -1) {
						users[j].follows.push(line[i]) //add user followed to the follows array
						users[j].follows.sort() //sort the follows array alphabetically
					}
				}
			}

			callback() //let the push function know it's done
		})

		q.push(lines, err => {
			//if the callback in queue function returns an error, 
			//it is catured here and console logged
			//this error will not stop the program, however
			if (err) {
				return console.log(err)
			}
			else {
				return
			}
		})

		q.drain = () => {
			done({
				success: true, 
				message: null, 
				data: users
			})
		}
	}
}

