'use strict'

const chai = require('chai')
const expect = chai.expect
const getTweets = require('./../modules/getTweets')

//the users array that should be returned by the getUsers function
const tweets = [
	'Alan> If you have a procedure with 10 parameters, you probably missed some.\r',
	'Ward> There are only two hard things in Computer Science: cache invalidation, naming things and off-by-1 errors.\r',
	'Alan> Random numbers should not be generated with a method chosen at random.\r',
	''
]

describe('receives an array of tweets, returns an array of users with their tweets', () => {
	it('If parameter valid: should return an object with keys: success, a boolean, message, a null, and data, an array', done => {
		getTweets(tweets, callback => {

			expect(callback).to.be.a('object')
			expect(callback).to.have.own.property('success')
			expect(callback).to.have.own.property('message')
			expect(callback).to.have.own.property('data')

			expect(callback.success).to.be.a('boolean')
			expect(callback.message).to.be.a('null')
			expect(callback.data).to.be.a('array')

			expect(callback.data[0]).to.have.own.property('user')
			expect(callback.data[0]).to.have.own.property('tweet')

			expect(callback.data[0]).to.be.a('object')
			expect(callback.data[0].user).to.be.a('string')
			expect(callback.data[0].tweet).to.be.a('string')

			done()
		})
	})

	it('If parameter invalid: should return an object with keys: success, a boolean, message, a string and data, an null', done => {
		getTweets(null, callback => {
			
			expect(callback).to.be.a('object')
			expect(callback).to.have.own.property('success')
			expect(callback).to.have.own.property('message')
			expect(callback).to.have.own.property('data')

			expect(callback.success).to.be.a('boolean')
			expect(callback.message).to.be.a('string')
			expect(callback.data).to.be.a('null')

			done()
		})
	})
})
