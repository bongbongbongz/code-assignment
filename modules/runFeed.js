/*
	This function generates the feed and prints it to the console

	This function requires the users array of object as well the tweets array of objects

	This function does not return anything and does not contain a callback function
*/

'use strict'

const _ = require('lodash')
const typeChecker = require('./typeChecker')

module.exports = (users, tweets, done) => {
	//checking if the parameter provided is of the required type
	if (!typeChecker(users, 'object') || !typeChecker(tweets, 'object')) {
		return done({
			success: false, 
			message: 'Please submit a parameter that is an javascript array or object', 
			data: null
		})
	}

	//checking if the parameter is not falsey
	if (!users || !tweets) {
		console.log('Please submit the array containing users, and the array containing tweets')
		
		return done({
			success: false, 
			message: 'Please submit the array containing users, and the array containing tweets', 
			data: null
		})
	}
	else {
		//going through the users array of objects
		return users.map(user => {
			console.log(user.user + '\n') //print the users name to the console and add a new line

			//going through the tweets array of obects
			tweets.map(tweet => {
				var i = _.indexOf(user.follows, tweet.user) //check if a user who tweeted is follow by the user whose feed is being generated
				if (i !== -1) console.log('\t @' + tweet.user + ': ' + tweet.tweet + '\n') //if true, render tweet on the user's feed/timeline
			})
		})
	}
}

