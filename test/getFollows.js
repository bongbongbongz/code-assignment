'use strict'

const chai = require('chai')
const expect = chai.expect
const getFollows = require('./../modules/getFollows')

//the users array that should be returned by the getUsers function
const users = [
	{user: 'Alan', follows: ['Alan']}, 
	{user: 'Martin', follows: ['Martin']}, 
	{user: 'Ward', follows: ['Ward']}, 
]

const lines = [
	['Ward', '', 'Alan'], 
	['Alan', '', 'Martin'], 
	['Ward', '', 'Martin', 'Alan'], 
	''
]

describe('receives an array of user names, returns an array of users with an array of who they follow', () => {
	it('If parameters valid: should return an object with keys: success, a boolean, message, a null, and data, an array', done => {
		getFollows(users, lines, callback => {

			expect(callback).to.be.a('object')
			expect(callback).to.have.own.property('success')
			expect(callback).to.have.own.property('message')
			expect(callback).to.have.own.property('data')

			expect(callback.success).to.be.a('boolean')
			expect(callback.message).to.be.a('null')
			expect(callback.data).to.be.a('array')

			expect(callback.data[0]).to.have.own.property('user')
			expect(callback.data[0]).to.have.own.property('follows')

			expect(callback.data[0]).to.be.a('object')
			expect(callback.data[0].user).to.be.a('string')
			expect(callback.data[0].follows).to.be.a('array')

			done()
		})
	})

	it('If parameters invalid: should return an object with keys: success, a boolean, message, a string and data, an null', done => {
		getFollows(null, null, callback => {
			
			expect(callback).to.be.a('object')
			expect(callback).to.have.own.property('success')
			expect(callback).to.have.own.property('message')
			expect(callback).to.have.own.property('data')

			expect(callback.success).to.be.a('boolean')
			expect(callback.message).to.be.a('string')
			expect(callback.data).to.be.a('null')

			done()
		})
	})
})
