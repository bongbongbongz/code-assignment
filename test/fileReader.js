'use strict'

const chai = require('chai')
const expect = chai.expect
const fileReader = require('./../modules/fileReader')

describe('Reads files from the data folder and returns them as an array', () => {
	it('If parameter valid: should return an object with keys: success, a boolean, message, a null, and data, an array', done => {
		fileReader('data/tweet.txt', callback => {

			expect(callback).to.be.a('object')
			expect(callback).to.have.own.property('success')
			expect(callback).to.have.own.property('message')
			expect(callback).to.have.own.property('data')

			expect(callback.success).to.be.a('boolean')
			expect(callback.message).to.be.a('null')
			expect(callback.data).to.be.a('array')

			expect(callback.data[0]).to.be.a('string')

			done()
		})
	})

	it('If parameter invalid: should return an object with keys: success, a boolean, message, a string and data, an null', done => {
		fileReader(null, callback => {
			
			expect(callback).to.be.a('object')
			expect(callback).to.have.own.property('success')
			expect(callback).to.have.own.property('message')
			expect(callback).to.have.own.property('data')

			expect(callback.success).to.be.a('boolean')
			expect(callback.message).to.be.a('string')
			expect(callback.data).to.be.a('null')

			done()
		})
	})
})
