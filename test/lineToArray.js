'use strict'

const chai = require('chai')
const expect = chai.expect
const lineToArray = require('./../modules/lineToArray')

//this is how the array should look like when coming from fileReader
const array = [
	'Ward follows Alan', 
	'Alan follows Martin', 
	'Ward follows Martin, Alan', 
	''
]

describe('expects an array of lines from the user.txt file, removes the "follows" word, then creates an array from each line', () => {
	it('If parameter valid: should return an object with keys: success, a boolean, message, a null, and data, an array', done => {
		lineToArray(array, callback => {

			expect(callback).to.be.a('object')
			expect(callback).to.have.own.property('success')
			expect(callback).to.have.own.property('message')
			expect(callback).to.have.own.property('data')

			expect(callback.success).to.be.a('boolean')
			expect(callback.message).to.be.a('null')
			expect(callback.data).to.be.a('array')
			expect(callback.data).to.deep.have.a('array')

			expect(callback.data[0]).to.be.a('array')

			expect(callback.data[0][0]).to.be.a('string')

			done()
		})
	})

	it('If parameter invalid: should return an object with keys: success, a boolean, message, a string and data, an null', done => {
		lineToArray(null, callback => {
			
			expect(callback).to.be.a('object')
			expect(callback).to.have.own.property('success')
			expect(callback).to.have.own.property('message')
			expect(callback).to.have.own.property('data')

			expect(callback.success).to.be.a('boolean')
			expect(callback.message).to.be.a('string')
			expect(callback.data).to.be.a('null')

			done()
		})
	})
})
